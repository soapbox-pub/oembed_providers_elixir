defmodule OEmbedProviders.Parser do
  @moduledoc false

  defmacro __using__(_opts) do
    quote do
      alias OEmbedProviders.Provider.Endpoint

      defp atomize_keys(map) when is_map(map) do
        Map.new(map, fn {k, v} -> {String.to_existing_atom(k), v} end)
      end
    end
  end
end

defmodule OEmbedProviders.Provider.Endpoint do
  @moduledoc """
  Represents an OEmbed endpoint.
  """
  use OEmbedProviders.Parser
  alias OEmbedProviders.Provider.Endpoint

  @type t :: %__MODULE__{
          schemes: [String.t()],
          url: String.t() | nil,
          formats: [String.t()],
          discovery: boolean() | nil
        }

  defstruct schemes: [], url: nil, formats: [], discovery: nil

  @doc """
  Convert a map into a `OEmbedProviders.Provider.Endpoint` struct.
  """
  @spec from_map(data :: map()) :: Endpoint.t()
  def from_map(data) when is_map(data) do
    data = atomize_keys(data)
    struct(Endpoint, data)
  end

  @doc """
  Check if the given URL matches this `Provider`.
  """
  @spec matches?(provider :: Endpoint.t(), url :: String.t()) :: boolean()
  def matches?(%Endpoint{schemes: schemes}, url) when is_list(schemes) do
    Enum.any?(schemes, fn scheme -> :glob.matches(url, scheme) end)
  end

  def matches?(_endpoint, _url), do: false
end

# OEmbed Providers Elixir

Compare URLs to a list of providers and get the OEmbed URL.
This allows you to avoid unnecessary HTTP requests and prevent being throttled by services such as YouTube.

```elixir
iex(1)> OEmbedProviders.oembed_url("https://www.youtube.com/watch?v=dQw4w9WgXcQ")
{:ok,
 "https://www.youtube.com/oembed?format=json&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DdQw4w9WgXcQ"}
iex(2)> OEmbedProviders.oembed_url("https://gleasonator.com/@alex")              
{:error, :no_provider}
```

OEmbed Providers is not a full client library on its own, but rather one piece of a complete OEmbed implementation.
It does not make HTTP requests or parse responses.
It ONLY constructs OEmbed URLs for known providers.

## Installation

The package can be installed
by adding `oembed_providers` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:oembed_providers, "~> 0.1.0"}
  ]
end
```

## Providers

Provider data is sourced from [`providers.json`](https://oembed.com/providers.json) and bundled with the library.

You can use a custom providers file in your application:

```elixir
import Config

config :oembed_providers,
  providers_file: "/absolute/path/to/file.json"
```

Be sure your JSON is valid and follows the same format as the original `providers.json`.

Note that the providers file is read **at compile time** and cannot be configured dynamically.

## License

OEmbed Providers Elixir is licensed under the MIT license.
See `LICENSE.md` for a full copy of the license.

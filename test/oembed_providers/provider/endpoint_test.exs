defmodule OEmbedProviders.Provider.EndpointTest do
  use ExUnit.Case
  alias OEmbedProviders.Provider.Endpoint

  @rick_roll "https://www.youtube.com/watch?v=dQw4w9WgXcQ"

  @youtube_endpoint %Endpoint{
    schemes: [
      "https://*.youtube.com/watch*",
      "https://*.youtube.com/v/*",
      "https://youtu.be/*"
    ],
    url: "https://www.youtube.com/oembed",
    formats: [],
    discovery: true
  }

  describe "from_map/1" do
    test "returns a struct representation" do
      endpoint = %{
        "schemes" => [
          "https://*.youtube.com/watch*",
          "https://*.youtube.com/v/*",
          "https://youtu.be/*"
        ],
        "url" => "https://www.youtube.com/oembed",
        "discovery" => true
      }

      assert Endpoint.from_map(endpoint) == @youtube_endpoint
    end
  end

  describe "matches?/2" do
    test "returns true for a matching URL" do
      assert Endpoint.matches?(@youtube_endpoint, @rick_roll)
    end

    test "returns false for a non-matching URL" do
      url = "https://gleasonator.com/@alex"
      refute Endpoint.matches?(@youtube_endpoint, url)
    end
  end
end

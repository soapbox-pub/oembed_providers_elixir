defmodule OEmbedProviders.ProviderTest do
  use ExUnit.Case
  alias OEmbedProviders.Provider
  alias OEmbedProviders.Provider.Endpoint

  @rick_roll "https://www.youtube.com/watch?v=dQw4w9WgXcQ"

  @youtube_provider %Provider{
    provider_name: "YouTube",
    provider_url: "https://www.youtube.com/",
    endpoints: [
      %Endpoint{
        schemes: [
          "https://*.youtube.com/watch*",
          "https://*.youtube.com/v/*",
          "https://youtu.be/*"
        ],
        url: "https://www.youtube.com/oembed",
        formats: [],
        discovery: true
      }
    ]
  }

  describe "from_map/1" do
    test "returns a struct representation" do
      provider = %{
        "provider_name" => "YouTube",
        "provider_url" => "https://www.youtube.com/",
        "endpoints" => [
          %{
            "schemes" => [
              "https://*.youtube.com/watch*",
              "https://*.youtube.com/v/*",
              "https://youtu.be/*"
            ],
            "url" => "https://www.youtube.com/oembed",
            "discovery" => true
          }
        ]
      }

      assert Provider.from_map(provider) == @youtube_provider
    end
  end

  describe "matches?/2" do
    test "returns true for a matching URL" do
      assert Provider.matches?(@youtube_provider, @rick_roll)
    end

    test "returns false for a non-matching URL" do
      url = "https://gleasonator.com/@alex"
      refute Provider.matches?(@youtube_provider, url)
    end
  end
end

defmodule OEmbedProvidersTest do
  use ExUnit.Case
  alias OEmbedProviders, as: Providers
  alias OEmbedProviders.Provider

  describe "list_providers/0" do
    test "returns a list of providers" do
      providers = Providers.list_providers()
      assert is_list(providers)
      assert [%Provider{provider_name: _} | _] = providers
    end
  end

  describe "find_provider/1" do
    test "returns the provider" do
      url = "https://www.youtube.com/watch?v=dQw4w9WgXcQ"
      assert %Provider{provider_name: "YouTube"} = Providers.find_provider(url)
    end

    test "returns nil if no provider exists" do
      url = "https://gleasonator.com/@alex"
      assert Providers.find_provider(url) == nil
    end
  end

  describe "oembed_url/1" do
    test "returns the OEmbed URL" do
      url = "https://www.youtube.com/watch?v=dQw4w9WgXcQ"

      expected =
        "https://www.youtube.com/oembed?format=json&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DdQw4w9WgXcQ"

      assert Providers.oembed_url(url) == {:ok, expected}
    end

    test "returns the OEmbed URL for an endpoint with {format}" do
      url = "https://vimeo.com/326933910"

      expected =
        "https://vimeo.com/api/oembed.json?format=json&url=https%3A%2F%2Fvimeo.com%2F326933910"

      assert Providers.oembed_url(url) == {:ok, expected}
    end

    test "returns an error for unsupported URL" do
      url = "https://gleasonator.com/@alex"
      assert Providers.oembed_url(url) == {:error, :no_provider}
    end
  end
end

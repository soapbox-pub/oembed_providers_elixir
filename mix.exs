defmodule OEmbedProviders.MixProject do
  use Mix.Project

  def project do
    [
      app: :oembed_providers,
      version: "0.1.0",
      elixir: "~> 1.11",
      name: "OEmbed Providers",
      description: "Get OEmbed URLs from a list of providers.",
      source_url: "https://gitlab.com/soapbox-pub/oembed_providers_elixir",
      package: package(),
      docs: docs(),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def package do
    [
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/soapbox-pub/oembed_providers_elixir"}
    ]
  end

  def docs do
    [
      main: "readme",
      extras: [
        "README.md",
        "LICENSE.md"
      ],
      groups_for_modules: groups_for_modules(),
      source_ref: "develop"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:glob, "~> 1.0"},
      {:jason, "~> 1.2"},
      {:ex_doc, "~> 0.24", only: :dev, runtime: false}
    ]
  end

  defp groups_for_modules do
    []
  end
end
